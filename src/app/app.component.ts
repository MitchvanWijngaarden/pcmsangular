import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { environment } from '@src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private deviceService: DeviceDetectorService) { }

  ngOnInit(): void {
    // if (this.deviceService.os != 'Windows') {
    //   environment.apiUrl = environment.apiMobileUrl
    // }
  }
}
