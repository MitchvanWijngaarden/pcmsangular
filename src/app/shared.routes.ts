import { Routes } from "@angular/router";

import { HomeComponent } from "@src/app/components/home/home.component";

import { AuthGuard } from "@src/app/_helpers/auth.guard";

import { RoleGuard } from "@src/app/_helpers/role.guard";
import { LoaderComponent } from "./components/modules/loader.component";

// import { RichtextComponent } from "@src/app/components/richtext/richtext.component.tns";

export const sharedRoutes: Routes = [
  {
      path: 'home',
      component: HomeComponent,
      canActivate: [AuthGuard] 
  },
  {
      path: 'user',
      loadChildren: () => import(`@src/app/components/user/user.module`).then(m => m.UserModule),
      canActivate: [AuthGuard] 
  },
  {
      path: 'portal',
      loadChildren: () => import(`@src/app/components/portal/portal.module`).then(m => m.PortalModule),
      canActivate: [AuthGuard, RoleGuard],
      data: {roles: [9002, ]}
  },
  // {
  //     path: 'text', component: RichtextComponent
  // },
  { 
      path: ':teamid',
      loadChildren: () => import(`@src/app/components/team/team.module`).then(m => m.TeamModule),
      canActivate: [AuthGuard] 
  },
];
