import  {ComponentFactoryResolver,Injectable, Inject, ReflectiveInjector} from '@angular/core';
import { DynamicComponent } from '@src/app/components/dynamic/dynamic.component';

@Injectable()
export class Service {
  factoryResolver: any;
  rootViewContainer: any;
  
  constructor(@Inject(ComponentFactoryResolver) factoryResolver) {
    this.factoryResolver = factoryResolver
  }
   
  setRootViewContainerRef(viewContainerRef) {
    this.rootViewContainer = viewContainerRef
  }
  
  addDynamicComponent() {
    const factory = this.factoryResolver.resolveComponentFactory(DynamicComponent)
    const component = factory.create(this.rootViewContainer.parentInjector)
    this.rootViewContainer.insert(component.hostView)
  }}