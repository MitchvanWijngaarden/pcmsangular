import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { NewsArticle } from '@src/app/_models/newsarticle';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class NewsArticleService {
    constructor(private http: HttpClient) { }

    getArticle(articleId: number) {
        return this.http.get<NewsArticle>(`${environment.apiUrl}/news/article/${articleId}`);
    }

    getIntranetArticles() {
        return this.http.get<NewsArticle[]>(`${environment.apiUrl}/news/article/`);
    }

    getArticlesByCategoryId(categoryId: number) {
        return this.http.get<NewsArticle[]>(`${environment.apiUrl}/news/category/${categoryId}/articles`);
    }

    getArticlesByCategoryIdLimit(categoryId: number, limit: number) {
        return this.http.get<NewsArticle[]>(`${environment.apiUrl}/news/category/${categoryId}/articles/${limit}`);
    }

    postArticle(article: NewsArticle) {
        // console.log(article);
        return this.http.post<NewsArticle>(`${environment.apiUrl}/news/article/`, { article }).pipe(map(article => {
            return article;
        }));
    }
    putArticle(article: NewsArticle, id: number) {
        // console.log(article);
        return this.http.put<NewsArticle>(`${environment.apiUrl}/news/article/${id}`, { article }).pipe(map(article => {
            return article;
        }));
    }
}
  