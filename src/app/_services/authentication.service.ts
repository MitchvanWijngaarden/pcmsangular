import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User, LoginUser } from '@src/app/_models/User';
import { environment } from '@src/environments/environment';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CustomStorageService } from '@src/app/_services/localstorage.service';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private storage: CustomStorageService, private deviceService: DeviceDetectorService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(this.storage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/auth/login`, { username, password })
            .pipe(map(user => {
                // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                // user.authdata = window.btoa(username + ':' + password);
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    // login(username: string, password: string) {
    //     let serverUrl = environment.apiUrl

    //     return this.http.post<any>(`${serverUrl}/auth/login`, { username, password }).subscribe(response => {
    //         if (response.username === username) {
    //             console.log(response)
    //             this.storage.setItem('currentUser', JSON.stringify(response));
    //             this.currentUserSubject.next(response);

    //             return response;
    //         }
    //     }, error => {
    //         alert('error:' + error);
    //     });
        
    //     // return this.http.post<any>(`${serverUrl}/auth/login`, { username, password })
        // .pipe(
        //     catchError(this.handleError<any[]>('loginUser', [])),
        //     map(user => {
        //             alert(user);
        //             // store user details and jwt token in local storage to keep user logged in between page refreshes
        //             this.storage.setItem('currentUser', JSON.stringify(user));
        //             this.currentUserSubject.next(user);
        //             return user;
        //         })
        // );
    // }

    logout() {
        // remove user from local storage to log user out
        this.storage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
      
          // TODO: send the error to remote logging infrastructure
          console.log(error); // log to console instead
      
          // TODO: better job of transforming error for user consumption
          console.log(`${operation} failed: ${error.message}`);
      
          // Let the app keep running by returning an empty result.
          return of(result as T);
        };
      }
}