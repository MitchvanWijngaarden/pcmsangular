import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { UserProfile } from '@src/app/_models/User';
import { Team } from '@src/app/_models/team';

@Injectable({ providedIn: 'root' })
export class UserProfileService {
    constructor(private http: HttpClient) { }

    getProfile(id: number) {
        const url = id === 0 ? `${environment.apiUrl}/users/me/profile` : `${environment.apiUrl}/users/${id}/profile`;
        return this.http.get<UserProfile>(url);
    }

    getTeams(id: number) {
        const url = id === 0 ? `${environment.apiUrl}/users/me/teams` : `${environment.apiUrl}/users/${id}/teams`;
        return this.http.get<Team>(url);
    }
}