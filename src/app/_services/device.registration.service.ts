import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Intranet } from '@src/app/_models/intranet';
import { environment } from '@src/environments/environment';
import * as uuid from 'nativescript-uuid'
import { map } from 'rxjs/internal/operators/map';
import { UserMacAddress, User, MacLogin } from '@src/app/_models/User';

@Injectable({ providedIn: 'root' })
export class DeviceRegistrationService {

  constructor(private http: HttpClient) {
  }

  registerDevice(intranet: Intranet) {
    let userMacAddress = new UserMacAddress();
    userMacAddress.mac_address = uuid.getUUID();
    userMacAddress.intra_id = intranet.intraId;
    userMacAddress.user_agent = "com.portalcms.iconcept.app";
    userMacAddress.platform = "Android";

    return this.http.post<UserMacAddress>(`${environment.apiUrl}/auth/mobile/mac/register`, { userMacAddress }).pipe(map(result => {
      return result;
    }));
  }

  loginDevice(intranet: Intranet) {
    let macLogin = new MacLogin();
    macLogin.mac_address = uuid.getUUID();
    macLogin.intra_id = intranet.intraId;

    return this.http.post<User>(`${environment.apiUrl}/auth/mobile/mac/login`, { macLogin }).pipe(map(result => {
      return result;
    }));
  }

  private createIntranetFromXML(intranet: Intranet, xml: any) {
    intranet.mobileUrl = xml.mobileurl._text
    intranet.intraId = xml.intraid._text
    intranet.categoryId = xml.categorieid._text

    return intranet
  }
}