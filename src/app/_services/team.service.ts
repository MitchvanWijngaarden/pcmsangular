import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { Team } from '@src/app/_models/team';
import { TeamUser } from '@src/app/_models/User';
import { NewsArticle } from '@src/app/_models/newsarticle';

@Injectable({ providedIn: 'root' })
export class TeamService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Team[]>(`${environment.apiUrl}/teams/`);
    }

    getMyTeams() {
        return this.http.get<Team[]>(`${environment.apiUrl}/users/me/teams`);
    }

    get(id: number) {
        return this.http.get<Team>(`${environment.apiUrl}/teams/${id}`);
    }

    getTeamMembers(id: number) {
        return this.http.get<TeamUser[]>(`${environment.apiUrl}/teams/${id}/members`);
    }

    getTeamArticles(id: number) {
        return this.http.get<NewsArticle[]>(`${environment.apiUrl}/teams/${id}/news/articles`);
    }
}