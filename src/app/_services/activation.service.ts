import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Intranet } from '@src/app/_models/intranet';
import { environment } from '@src/environments/environment';
import { CustomStorageService } from '@src/app/_services/localstorage.service';
import { Router } from '@angular/router';
import * as xml from 'xml-js'
import { map } from 'rxjs/internal/operators/map';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ActivationService {
  public currentIntranetSubject: BehaviorSubject<Intranet>;
  public currentIntranet: Observable<Intranet>;
  public error = "nothing"

  constructor(
    private http: HttpClient,
    private storage: CustomStorageService,
    private router: Router,
  ) {
    this.currentIntranetSubject = new BehaviorSubject<Intranet>(JSON.parse(this.storage.getItem('currentIntranet')));
    this.currentIntranet = this.currentIntranetSubject.asObservable();
  }

  public get currentIntranetValue(): Intranet {
    return this.currentIntranetSubject.value;
  }

  activate(activationCode: string): Observable<any> {
    return this.http.get(`${environment.activationUrl}${activationCode}`, {responseType: 'text'}).pipe(map(data => {
        const convertedXML = xml.xml2js(data, {compact: true});
        const result = convertedXML['Result'];

        if (result.response._text != "failed") {
          let intranet = new Intranet()
          intranet = this.createIntranetFromXML(intranet, result)

          localStorage.setItem('currentIntranet', JSON.stringify(intranet));
          this.currentIntranetSubject.next(intranet);
          return intranet
        }

        return null
      }
    ));
  }

  private createIntranetFromXML(intranet: Intranet, xml: any) {
    intranet.mobileUrl = xml.mobileurl._text
    intranet.intraId = xml.intraid._text
    intranet.categoryId = xml.categorieid._text

    return intranet
  }
}