import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { Widget } from '@src/app/_models/widget';


@Injectable({ providedIn: 'root' })
export class WidgetService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Widget[]>(`${environment.apiUrl}/widgets/`);
    }

    getWidgetsForPosition(position: string) {
        console.log(environment.apiUrl)
        return this.http.get<Widget[]>(`${environment.apiUrl}/widgets/${position}`);
    }
}