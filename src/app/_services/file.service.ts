import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { UserFile } from '../_models/file';

@Injectable({ providedIn: 'root' })
export class FileService {
    constructor(private http: HttpClient) { }

    getMyFiles() {
        return this.http.get<UserFile[]>(`${environment.apiUrl}/users/me/files`);
    }
}