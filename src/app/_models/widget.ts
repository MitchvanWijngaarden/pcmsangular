export class Widget {
  id: number;
  name: string;
  description: string;
  arguments: string;
  htmlcontent: string;
  zIndex: number;
  widgetId: number;
}