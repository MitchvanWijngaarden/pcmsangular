export class User {
  id: number;
  intra_id: number;
  email: string;
  username: string;
  token?: string;
  permissions: Permission[];
}

export class Permission {
  id: number
}


export class LoginUser {
  id: number;
  intra_id: number;
  email: string;
  username: string;
  gender: string;
  b_name: string;
  full_name: string;
  nickname: string;
  initials: string;
  surname_prefix: string;
  surname: string;
  profile: string;
  token: string;
}


export class TeamUser {
  id: number;
  full_name: string;
  phone_number: string;
  email: string;
  place_of_employment: string;
}

export class UserProfile{
  full_name: string;
}

export class UserMacAddress {
  user_id: number;
  mac_address: string;
  notification_access_key: string;
  user_agent: string;
  expiration_date: string;
  last_login: string;
  subscription: string;
  platform: string;
  intra_id: number;
}

export class MacLogin {
  mac_address: string;
  intra_id: number;
}