export class NewsArticle {
  id: number;
  title: string;
  tocus: string;
  summary: string;
  summary_image: string;
  date_published: string;
  date_expired: string;
  date_modified: string;
  date_created: string;
  user_published: number;
  category_id: number;
  category: NewsCategory;
  publish_rss: number;
  data_id: number;
  intra_id: number;
}

export class NewsCategory {
  id: number;
  name: string;
  team_id: number;
}