import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
// import { ActivationComponent } from '@src/app/components/activation/activation.component';
import { LoginComponent } from '@src/app/components/login/login.component';
import { sharedRoutes } from '@src/app/shared.routes';
import { Routes } from '@angular/router';


export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    // { path: 'activation', component: ActivationComponent },
    { path: 'login', component: LoginComponent },
];

@NgModule({
    imports: [
        NativeScriptRouterModule.forRoot(
          routes,
          { enableTracing: true } // <-- debugging purposes only
        ),
        NativeScriptRouterModule.forRoot(
            sharedRoutes,
            // { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }