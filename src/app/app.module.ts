import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from '@src/app/app.component';
import { AppRoutingModule } from '@src/app/app.routes';
import { HomeComponent } from '@src/app/components/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { AuthGuard } from "@src/app/_helpers/auth.guard";
import { LoginComponent } from '@src/app/components/login/login.component';
import { CustomStorageService } from '@src/app/_services/localstorage.service';
import { NavbarService } from '@src/app/_services/navbar.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationModule } from '@src/app/components/navigation/navigation.module';

import { MatSliderModule } from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatButtonModule} from '@angular/material/button'; 
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import { MatGridListModule } from '@angular/material';
import { JwtInterceptor } from '@src/app/_helpers/jwt.interceptor';
import { TeamService } from '@src/app/_services/team.service';
import { UserProfileService } from '@src/app/_services/user.profile.service';
import { TeamComponent } from '@src/app/components/team/team.component';
import { FileService } from '@src/app/_services/file.service';
import { NewsArticleService } from '@src/app/_services/news.article.service';
import { RoleGuard } from '@src/app/_helpers/role.guard';
import { WidgetService } from '@src/app/_services/widget.service';
import { RichtextComponent } from '@src/app/components/richtext/richtext.component';
import { ActivationComponent } from '@src/app/components/activation/activation.component';
import { ActivationService } from '@src/app/_services/activation.service';
import { ViewComponent } from '@src/app/components/view/view.component';
import { ServiceBasedModule } from './components/modules/service-based.module';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        TeamComponent,
        RichtextComponent,
        ActivationComponent,
        ViewComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule, ReactiveFormsModule,
        DeviceDetectorModule.forRoot(),
        NoopAnimationsModule,
        MatSliderModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatCardModule,
        MatInputModule,
        MatGridListModule,
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        NavigationModule,
        ServiceBasedModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        AuthGuard,
        RoleGuard,
        CustomStorageService,
        NavbarService,
        TeamService,
        UserProfileService,
        FileService,
        WidgetService,
        NewsArticleService,
        ActivationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }