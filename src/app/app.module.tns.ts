import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { AppRoutingModule } from '@src/app/mobile.routes';
import { AppComponent } from '@src/app/app.component';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { AuthGuard } from "@src/app/_helpers/auth.guard";


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
import { NativeScriptHttpClientModule} from 'nativescript-angular/http-client';

import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { LoginComponent } from '@src/app/components/login/login.component';
import { CustomStorageService } from '@src/app/_services/localstorage.service';
import * as mobileStorage from 'nativescript-localstorage';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NavbarService } from '@src/app/_services/navbar.service';
import { HomeComponent } from '@src/app/components/home/home.component';
import { WebviewModuleComponent } from '@src/app/components/webview-module/webview-module.component';
import { TeamComponent } from '@src/app/components/team/team.component';
import { JwtInterceptor } from '@src/app/_helpers/jwt.interceptor';
// import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NavigationModule } from '@src/app/components/navigation/navigation.module';
import { PortalComponent } from '@src/app/components/portal/portal.component';
import { RichtextComponent } from '@src/app/components/richtext/richtext.component.tns';
import { RoleGuard } from '@src/app/_helpers/role.guard';
// import { ActivationComponent } from '@src/app/components/activation/activation.component';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { DeviceRegistrationService } from '@src/app/_services/device.registration.service';
import { ViewComponent } from '@src/app/components/view/view.component';
import { ServiceBasedModule } from './components/modules/service-based.module';
@NgModule({
  declarations: [
    LoginComponent,
    AppComponent,
    HomeComponent,
    WebviewModuleComponent,
    TeamComponent,
    PortalComponent,
    RichtextComponent,
    // ActivationComponent,
    ViewComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    NativeScriptFormsModule,
    // NativeScriptHttpClientModule,
    // NativeScriptUISideDrawerModule,
    DeviceDetectorModule.forRoot(),
    NavigationModule,
    ServiceBasedModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AuthGuard,
    {
      provide: CustomStorageService,
      useValue: mobileStorage 
    },
    NavbarService,
    AuthenticationService,
    DeviceRegistrationService,
    RoleGuard,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
