import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop'

import { WidgetService } from '@src/app/_services/widget.service';
import { Widget } from '@src/app/_models/widget';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
  loadingAvailableWidgets: boolean;
  availableWidgets: Widget[];
  todo: Widget[] = [];
  done: Widget[] = [];
  weeks = [];
  connectedTo = [];

  widgets = [];

  columns= [];


  rows: any;

  constructor(
    private widgetService: WidgetService,
    private httpClient: HttpClient,
  ) { }


  ngOnInit() {
    this.loadingAvailableWidgets = true;
    this.widgetService.getAll().pipe(first()).subscribe(widgets => {
      this.loadingAvailableWidgets = false;
      this.availableWidgets = widgets;
    });

    this.httpClient.get<any>("assets/template.properties.json").subscribe(data =>{;
      this.rows = data.rows
      console.log('kaas', data)
    })
  }

  drop(event: CdkDragDrop<Widget[]>) {

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      if(event.previousContainer.id === "widgets") {
        copyArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      } else {
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }

    }
  }

  noReturnPredicate() {
    return false;
  }

}