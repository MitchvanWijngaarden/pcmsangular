import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { PortalRoutingModule } from '@src/app/components/portal/portal-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { ArticleComponent } from '@src/app/components/portal/article/article.component';
import { ListComponent } from '@src/app/components/portal/article/list/list.component';
import { EditComponent } from '@src/app/components/portal/article/edit/edit.component.tns';
import { WidgetComponent } from '@src/app/components/portal/widget/widget.component';
import { ManageComponent } from '@src/app/components/portal/widget/manage/manage.component';


@NgModule({
  declarations: [ArticleComponent, ListComponent, EditComponent, WidgetComponent, ManageComponent],
  imports: [
    PortalRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PortalModule { }
