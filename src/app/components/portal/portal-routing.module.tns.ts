import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { ListComponent } from '@src/app/components/portal/article/list/list.component';
import { EditComponent } from '@src/app/components/portal/article/edit/edit.component.tns';
import { ManageComponent } from '@src/app/components/portal//widget/manage/manage.component';


const routes: Routes = [
  {
    path: 'article/list',
    component: ListComponent
  },
  {
    path: 'article/edit/:articleid',
    component: EditComponent
  },
  {
    path: 'widget/manage',
    component: ManageComponent
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class PortalRoutingModule { }
