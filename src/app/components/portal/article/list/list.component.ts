import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { NewsArticle } from '@src/app/_models/newsarticle';
import { Team } from '@src/app/_models/team';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { NewsArticleService } from '@src/app/_services/news.article.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  displayedColumns: string[] = ['title', 'date_published', 'date_expired', 'category', 'id'];
  dataSource: MatTableDataSource<NewsArticle>;

  pageSizeOptions: number[];

  loadingHome = false;
  loadingMembers = false;

  teamId: number;
  team: Team;
  teamNewsArticles: NewsArticle[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private newsService: NewsArticleService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngAfterViewInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      this.teamId = +params.get('teamid');
    });

    this.loadingMembers = true;

    this.newsService.getIntranetArticles().pipe(first()).subscribe(teamMembers => {
      this.loadingMembers = false;
      this.teamNewsArticles = teamMembers;
      this.dataSource = new MatTableDataSource<NewsArticle>(this.teamNewsArticles);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.pageSizeOptions = [10, 25, 50, this.dataSource.data.length];
    });
  }
}