import { Component, OnInit } from '@angular/core';
import { NewsArticle } from '@src/app/_models/newsarticle';
import { NewsArticleService } from '@src/app/_services/news.article.service';
import { first } from 'rxjs/operators';
import { FormGroup, FormBuilder } from '@angular/forms';

import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  angForm: FormGroup;
  loadingArticle = false;
  articleId: number;
  article: NewsArticle;
  
  constructor(private fb: FormBuilder, private newsService: NewsArticleService, private activatedRoute: ActivatedRoute) {

  }


  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '500',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      {class: 'arial', name: 'Arial'},
      {class: 'times-new-roman', name: 'Times New Roman'},
      {class: 'calibri', name: 'Calibri'},
      {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
      // {
      //   name: 'quote',
      //   class: 'quote',
      // },
      // {
      //   name: 'redText',
      //   class: 'redText'
      // },
      // {
      //   name: 'titleText',
      //   class: 'titleText',
      //   tag: 'h1',
      // },
    ],
    uploadUrl: 'http://localhost:5000/api/v1/files/images',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    // toolbarHiddenButtons: [
    //   ['bold', 'italic'],
    //   ['fontSize']
    // ]
  };

  createForm() {
    // console.log(this.article.summary)
    this.angForm = this.fb.group({
      title: [this.article.title],
      tocus: [this.article.tocus],
      summary: [this.article.summary],
      date_published: new Date(this.article.date_published),
      // date_published: [this.article.date_published ? this.article.date_published : 'test'],
      date_expired: new Date(this.article.date_expired),
      category_id: [''],
    });
  }

  convertDate(date) {
    return new Date(date.toISOString()).toLocaleString("en-us", {timeZone: "Europe/Amsterdam"});
  }


  save() {
    const formData = this.angForm.value
    let article = new NewsArticle()


    article.title = formData.title;
    article.tocus = formData.tocus;
    article.category_id = 10;
    article.summary = formData.summary;
    article.summary_image = "test";
    article.user_published = 10993;
    article.intra_id = 71;
    article.publish_rss = 0;
    article.data_id = 0;
    article.date_expired =  this.convertDate(formData.date_expired);
    article.date_published =  this.convertDate(formData.date_published);


    this.newsService.putArticle(article, this.articleId)
    .pipe(first())
    .subscribe(
        data => {
            console.log(data)
        },
        error => {
            console.log(error);
        });
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.articleId = +params.get('articleid');
    });
  
    this.loadingArticle = true;
    this.newsService.getArticle(this.articleId).pipe(first()).subscribe(article => {
      this.loadingArticle = false;
      this.article = article;

      this.createForm();
    });

  } 
}