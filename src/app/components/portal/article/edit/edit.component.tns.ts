import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { WebView, LoadEventData } from 'tns-core-modules/ui/web-view';
import { WebViewInterface } from 'nativescript-webview-interface';
import * as fs from "tns-core-modules/file-system";
import { NativeToJSEventData } from '@src/app/_helpers/richtext_helper';
import { NewsArticle } from '@src/app/_models/newsarticle';
import { NewsArticleService } from '@src/app/_services/news.article.service';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  loadingArticle = false;
  articleId: number;
  article: NewsArticle;
  content: any;

  oWebViewInterface:any;
  private webViewElement: WebView;

  @ViewChild("webView", { static: false }) webViewRef: ElementRef;
  
  constructor(private newsService: NewsArticleService, private activatedRoute: ActivatedRoute, private changeDetector : ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.webViewElement = this.webViewRef.nativeElement;
    this.initWebView();
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.articleId = +params.get('articleid');
    });

    this.newsService.getArticle(this.articleId).pipe(first()).subscribe(article => {
      this.article = article;
    });
  }  

  initWebView() {
    let webViewSrc=  encodeURI(`${fs.knownFolders.currentApp().path}/assets/content.html`);
    this.oWebViewInterface = new WebViewInterface(this.webViewElement, webViewSrc);
    this.loadingArticle = true;

    this.webViewElement.on(WebView.loadStartedEvent, function (args: LoadEventData) {
      if (this.android) {
        this.android.getSettings().setSupportZoom(false);
        this.android.getSettings().setBuiltInZoomControls(false);
        this.android.getSettings().setDisplayZoomControls(false);
      }
    });

    this.webViewElement.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
      if (!args.error) {
          console.log(this.article)
          this.oWebViewInterface.emit('javaScriptEvent', new NativeToJSEventData(4, this.article.tocus));
          this.loadingArticle = false;
          this.changeDetector.detectChanges();

          this.oWebViewInterface.on('nativeScriptEvent', (event)=>{
              let mode = event.mode;
              if (mode == 1) {
                  // this.linkInputPrompt()
              } else if (mode == 2){
                  // this.pickImage();
              } else if (mode == 3) {
                  // this.videoLinkInputPrompt();
              } else {
                   console.dir(event.data);
              }
              console.dir(event);
          })
      } else {
          console.log('error');
          console.dir(args.error)
      }
    });
  }

  setContent(arg0: string) {
    this.oWebViewInterface.emit('javaScriptEvent', new NativeToJSEventData(4, arg0));
  }

  async getContent() {
      this.oWebViewInterface.callJSFunction('getContent', [],
      (r: any)=>{

        console.log(r.content)

        let article = new NewsArticle()


    article.title = "test"
    article.tocus = r.content
    article.category_id = 100;
    article.summary = "test"
    article.summary_image = "test";
    article.user_published = 9908;
    article.intra_id = 71;
    article.publish_rss = 0;
    article.data_id = 0;
    article.date_expired = "2020-03-11 00:00:00"
    article.date_published =  "2020-03-11 00:00:00"


    this.newsService.putArticle(article, this.articleId)
    .pipe(first())
    .subscribe(
        data => {
            console.log(data)
        },
        error => {
            console.log(error);
        });


        // TODO: edit post?
      })
  }

  async onSubmit() {
    this.getContent();
  }

// ngOnDestroy(){
//     this.changeDetectorRef.detach();
// }

}
