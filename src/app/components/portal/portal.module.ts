import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortalRoutingModule } from '@src/app/components/portal/portal-routing.module';
import { PortalComponent } from '@src/app/components/portal/portal.component';
import { ArticleComponent } from '@src/app/components/portal/article/article.component';
import { ListComponent } from '@src/app/components/portal/article/list/list.component';
import { EditComponent } from '@src/app/components/portal/article/edit/edit.component';
import { MatTabsModule, MatIconModule, MatButtonModule, MatListModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from 'ngx-mat-datetime-picker';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { WidgetComponent } from '@src/app/components/portal/widget/widget.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { ManageComponent } from '@src/app/components/portal/widget/manage/manage.component';



@NgModule({
  declarations: [
    PortalComponent,
    ArticleComponent,
    ListComponent,
    EditComponent,
    WidgetComponent,
    ManageComponent,
  ],
  imports: [
    CommonModule,
    PortalRoutingModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatTimepickerModule,
    NgxMatDatetimePickerModule,
    AngularEditorModule,
    DragDropModule
    
    
  ]
})
export class PortalModule { }
