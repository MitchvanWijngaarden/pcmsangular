import { Component, Input, OnInit } from '@angular/core';
import { WidgetService } from '@src/app/_services/widget.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'view-collection',
  template: '<p>{{getView}} called</p>',
})

export class ViewComponent implements OnInit {
  @Input() getView: string;

  constructor(private widgetService: WidgetService) {

  }

  ngOnInit(): void {


    this.widgetService.getWidgetsForPosition(this.getView).pipe(first()).subscribe(widgets => {
      for (let widget of widgets) {
        // TODO: create views which match arguments?
        // https://medium.com/front-end-weekly/dynamically-add-components-to-the-dom-with-angular-71b0cb535286
      }
    });
  }
}