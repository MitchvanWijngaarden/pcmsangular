import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WebView, LoadEventData } from 'tns-core-modules/ui/web-view';
import { WebViewInterface } from 'nativescript-webview-interface';
import * as fs from "tns-core-modules/file-system";
import { NativeToJSEventData } from '@src/app/_helpers/richtext_helper';

@Component({
  selector: 'app-richtext',
  templateUrl: './richtext.component.html',
  styleUrls: ['./richtext.component.css']
})
export class RichtextComponent implements OnInit {
  oWebViewInterface:any;
  private webViewTest: WebView;

  @ViewChild('webView',  { static: false }) webView: ElementRef;

  @ViewChild("webView", { static: false }) webViewRef: ElementRef;
  constructor() { }

  ngAfterViewInit() {
    this.webViewTest = this.webViewRef.nativeElement;
    this.initWebView();
  }

  ngOnInit() {
  }

  initWebView() {
    let webViewSrc=  encodeURI(`${fs.knownFolders.currentApp().path}/assets/content.html`);
    // let webviewN = this.webView.nativeElement;
    this.oWebViewInterface = new WebViewInterface(this.webViewTest, webViewSrc);

    this.webViewTest.on(WebView.loadStartedEvent, function (args: LoadEventData) {
      if (this.android) {
        this.android.getSettings().setSupportZoom(false);
        this.android.getSettings().setBuiltInZoomControls(false);
        this.android.getSettings().setDisplayZoomControls(false);
      }
    });

    this.webViewTest.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
      if (!args.error) {
          // this.setContent(`<blockquote>Collaborate with Experts for Consultations, Projects, Workshops, Training &amp; more, on-demand only on <b style="font-style: normal;">TapChief</b></blockquote><br>`);

          this.oWebViewInterface.emit('javaScriptEvent', new NativeToJSEventData(4, 'test?'));

          this.oWebViewInterface.on('nativeScriptEvent', (event)=>{
              let mode = event.mode;
              if (mode == 1) {
                  // this.linkInputPrompt()
              } else if (mode == 2){
                  // this.pickImage();
              } else if (mode ==3) {
                  // this.videoLinkInputPrompt();
              } else {
                   console.dir(event.data);
              }
              console.dir(event);
          })
      } else {
          console.log('error');
          console.dir(args.error)
      }
    });
  }

  setContent(arg0: string) {
    this.oWebViewInterface.emit('javaScriptEvent', new NativeToJSEventData(4, arg0));
  }

  getContent() {
      this.oWebViewInterface.callJSFunction('getContent', [],(r)=>{console.log(r)})
  }

// ngOnDestroy(){
//     this.changeDetectorRef.detach();
// }

}
