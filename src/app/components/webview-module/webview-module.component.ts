import { Component, OnInit } from '@angular/core';
import { LoadEventData, WebView } from 'tns-core-modules/ui/web-view/web-view';
import { ActivatedRoute, Params } from '@angular/router';

import * as application from "tns-core-modules/application";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "tns-core-modules/application";

@Component({
	selector: 'app-webview-module',
	templateUrl: './webview-module.component.html',
	styleUrls: ['./webview-module.component.css']
})

export class WebviewModuleComponent implements OnInit {
	webViewSrc = "http://swift.iconcept.nl/mobile_update2/?deviceId=539c1a83b0ff8652&intraId=1&categorie=880&hidemenu=1&mobileoptions=";
	module: string;
	constructor(private route: ActivatedRoute) { }

	ngOnInit(): void {
		this.route.params.subscribe(
			(params: Params) => {
				this.module = params["id"];
				this.webViewSrc = "http://swift.iconcept.nl/mobile_update2/?deviceId=539c1a83b0ff8652&intraId=1&categorie=880&hidemenu=1&mobileoptions=&module=" + this.module;
			}
		);
	}

	onLoadStarted(args: LoadEventData) {
		const webView = args.object as WebView;
		// const utils = require("tns-core-modules/utils/utils");

		if (!args.error) {
			// TODO: Add in global app url
			// TODO: if url not global app url
			if (args.url.includes("swagger.json")) {
				webView.stopLoading();
				// utils.openUrl(args.url);
			}
		} else {
				console.log(`EventName: ${args.eventName}`);
				console.log(`Error: ${args.error}`);
		}

		if (webView.android) {
			webView.android.getSettings().setSupportZoom(false);
			webView.android.getSettings().setBuiltInZoomControls(false);
			webView.android.getSettings().setDisplayZoomControls(false);
		}
	}

	onLoadFinished(args: LoadEventData):any {
		const webView = args.object as WebView;

		if (args.error) {
				console.log(`EventName: ${args.eventName}`);
				console.log(`Error: ${args.error}`);
		}
		if (application.android) {
			application.android.on(application.AndroidApplication.activityBackPressedEvent, (args:AndroidActivityBackPressedEventData) => {      
				// if (webView.canGoBack) {
				// 	alert("go back");
				// 	webView.goBack();
				// 	args.cancel = true;
				// }
				// } else{
				// 	// -> remove the event listener
				// 	application.android.off(application.AndroidApplication.activityBackPressedEvent);
				// }
				// args.cancel = true;
			});
		}
	}
}