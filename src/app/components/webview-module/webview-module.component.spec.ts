import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebviewModuleComponent } from '@src/app/webview-module/webview-module.component';

describe('WebviewModuleComponent', () => {
  let component: WebviewModuleComponent;
  let fixture: ComponentFixture<WebviewModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebviewModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebviewModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
