import { OnInit, Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "@src/app/_services/authentication.service";
import { first } from "rxjs/operators";

@Component({
  selector: "login",
  templateUrl: './login.component.html',
}) 

export class LoginComponent implements OnInit {
  returnUrl: string ;
  error = '';

  public username: string;
  public password: string;

  loading = false;
  submitted = false;

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    // alert("Mobile only");
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
) { 
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
        this.router.navigate(['/home']);
    }
}

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    // todo if android
    // if (this.loginForm.invalid) {
    //     return;
    // }

    this.loading = true;
    this.authenticationService.login(this.username, this.password).pipe(first()).subscribe(data => {
      this.router.navigate([this.returnUrl]);
    },
    error => {
      this.error = error;
      console.log(error)
      this.loading = false;
    });
  } 
}