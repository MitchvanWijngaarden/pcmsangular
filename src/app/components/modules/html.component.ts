import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-html',
  templateUrl: './html.component.html',
  // template: `
  //   <div>HTML goes here</div>
  // `,
})
export class HtmlComponent  {
  @Input() html: string = "<b>failure</b>";
}
