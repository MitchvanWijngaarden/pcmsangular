import { Component, Input, OnInit } from '@angular/core';
import { NewsArticleService } from '@src/app/_services/news.article.service';
import { first } from 'rxjs/internal/operators/first';
import { NewsArticle } from '@src/app/_models/newsarticle';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
})
export class NewsComponent implements OnInit {
  @Input() widgetArguments: string;

  blockTitle: string;
  categoryId: number;
  articleLimit: number;
  loadingArticles: boolean = false;
  articles: NewsArticle[];
  error: string;

  constructor(private newsArticleService: NewsArticleService) { }

  ngOnInit(): void {
    const argumentsJSON = JSON.parse(this.widgetArguments);
    const args = argumentsJSON.ARGUMENTS;
    // console.log(args);

    this.blockTitle = args.blockTitle.value;
    this.categoryId = args.categoryId.value;
    this.articleLimit = args.Limit.value;

    this.loadingArticles = true;

    console.log(this.categoryId)

    this.newsArticleService.getArticlesByCategoryIdLimit(this.categoryId, this.articleLimit).pipe(first()).subscribe(articles => {
      this.loadingArticles = false;
      this.articles = articles;
    }, error => {
      // console.log(error)
      this.error=(error)
    });
  }
}