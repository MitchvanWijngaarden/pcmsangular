import { 
  Component, 
  OnInit,
  ViewChild,
  ViewContainerRef,
  Input,
} from '@angular/core';
import { FactoryService } from "./factory.service";
import { first } from 'rxjs/internal/operators/first';
import { WidgetService } from '@src/app/_services/widget.service';

@Component({
  selector: 'widgets',
  template: `<ng-container #dynamic></ng-container>`,
})
export class LoaderComponent implements OnInit { 
  @ViewChild("dynamic", { static:true, read: ViewContainerRef }) view: ViewContainerRef;
  @Input() position: string;

  constructor(private service: FactoryService, private widgetService: WidgetService) { }

  ngOnInit() {
    this.service.setRootViewContainerRef(this.view);
    this.service.reset();
    this.getWidgets(this.position)
  }

  getWidgets(position: string) {
    this.widgetService.getWidgetsForPosition(position).pipe(first()).subscribe(widgets => {
      // console.log(widgets)
      for (let widget of widgets) {
        if (widget.widgetId == 7) {
          console.log(widget)
          this.news(widget);
        }
        if (widget.widgetId == 2) {
          this.html();
        }
      }
    });
  }

  news(widget: any): void {
    // this.service.reset();
    this.service.insertNewsComponent(widget);
  }

  html(): void {
    // this.service.reset();
    this.service.insertHTMLComponent();
  }
}
