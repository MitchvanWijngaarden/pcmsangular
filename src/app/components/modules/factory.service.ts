import { 
  Injectable,
  ComponentFactoryResolver,
  ComponentFactory,
  ViewContainerRef,
} from '@angular/core';
import { NewsComponent } from "./news.component";
import { HtmlComponent } from "./html.component";

@Injectable()
export class FactoryService {
  rootViewContainer: ViewContainerRef;

  constructor(private factoryResolver: ComponentFactoryResolver) { }

  setRootViewContainerRef(view: ViewContainerRef): void {
    this.rootViewContainer = view;
  }

  reset(): void {
    this.rootViewContainer.clear();
  }

  insertNewsComponent(widget:any): void {
    let instance: any = this.insertComponent(NewsComponent);
    instance.widgetArguments = widget.arguments;
  }

  insertHTMLComponent(): void {
    let instance: any = this.insertComponent(HtmlComponent);
    instance.html = "<b>ff</b>"
  }

  private insertComponent(componentType): void {
    const factory: ComponentFactory<any> = this.factoryResolver.resolveComponentFactory(componentType);
    let ref = this.rootViewContainer.createComponent(factory);

    return ref.instance;
  }
}