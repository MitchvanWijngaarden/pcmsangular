import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';  
import { NewsComponent } from "./news.component";
import { HtmlComponent } from "./html.component";
import { FactoryService } from './factory.service';
import { LoaderComponent } from "./loader.component";
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LoaderComponent,
    NewsComponent,
    HtmlComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  entryComponents: [
    NewsComponent,
    HtmlComponent,
  ],
  exports: [
    LoaderComponent,
  ],
  providers: [
    FactoryService,
  ],
})
export class ServiceBasedModule { }