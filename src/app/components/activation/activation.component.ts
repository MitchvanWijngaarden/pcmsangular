import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivationService } from '@src/app/_services/activation.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavbarService } from '@src/app/_services/navbar.service';
import { first } from 'rxjs/internal/operators/first';
import { environment } from '@src/environments/environment';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {
  activationForm: FormGroup;
  error: string

  constructor(
    private activationService: ActivationService,
    private formBuilder: FormBuilder, 
    private router: Router,
    public nav: NavbarService,
  ) {
    if (this.activationService.currentIntranetValue) { 
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.activationForm = this.formBuilder.group({
      activationcode: ['', Validators.required]
    });
  }

  get f() { return this.activationForm.controls; }

  onSubmit() {
    this.activationService.activate(this.f.activationcode.value).pipe(first()).subscribe(intranet => {

      if(intranet) {
        console.log(intranet)
        environment.apiMobileUrl = intranet.mobileUrl;
        this.router.navigate(['/login']);
      }

      this.error = "The provided activation code is incorrect, please try again."
    });
  }
}
