import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivationService } from '@src/app/_services/activation.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavbarService } from '@src/app/_services/navbar.service';
import { first } from 'rxjs/internal/operators/first';
import { environment } from '@src/environments/environment';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { DeviceRegistrationService } from '@src/app/_services/device.registration.service';
import { RouterExtensions } from 'nativescript-angular/router';


@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {
  error: string

  public activationCode: string;

  constructor (
    private activationService: ActivationService,
    private authenticationService: AuthenticationService,
    private deviceRegistrationServe: DeviceRegistrationService,
    private routerExtensions: RouterExtensions,
    public nav: NavbarService,
  ) {
    if (this.activationService.currentIntranetValue) { 
      const intranet = this.activationService.currentIntranetValue
      environment.apiUrl = intranet.mobileUrl;

      this.deviceRegistrationServe.loginDevice(intranet).pipe(first()).subscribe(data => {
        environment.apiUrl = intranet.mobileUrl;
        console.log(environment.apiUrl)
        this.authenticationService.currentUserSubject.next(data)
        console.log("going home")
        this.routerExtensions.navigate(['/home'],{ clearHistory: true });
      },
      error => {
        console.log(error)
        console.log("error auto login")
      });

      // this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    
  }

  onSubmit() {
    this.activationService.activate(this.activationCode).pipe(first()).subscribe(intranet => {

      if(intranet) {
        console.log(intranet)
        environment.apiUrl = intranet.mobileUrl;
        console.log(environment.apiUrl)

        this.deviceRegistrationServe.loginDevice(intranet).pipe(first()).subscribe(data => {
          this.authenticationService.currentUserSubject.next(data)
          console.log("going home")
          this.routerExtensions.navigate(['/home'],{ clearHistory: true });
        },
        error => {
          console.log(error)
          this.deviceRegistrationServe.registerDevice(intranet).pipe(first()).subscribe(data => {
            console.log("going login")
            this.routerExtensions.navigate(['/login'],{ clearHistory: true });
          },
          error => {
            this.error = "Could not register device, please try again."
            console.log(error)
          });
        });
      }
    },
    error => {
      this.error = "The provided activation code is incorrect, please try again."
      console.log(error)
    });
  }
}
