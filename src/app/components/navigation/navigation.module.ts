import { NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NavbarService } from '@src/app/_services/navbar.service';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from '@src/app/components/navigation/navigation.component';

@NgModule({

  declarations: [
    NavigationComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatSliderModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,

  ],
  exports: [
    NavigationComponent
  ],
  providers: [
    NavbarService
  ],
})


export class NavigationModule { }