
import { NavigationComponent } from '@src/app/components/navigation/navigation.component';
import { NgModule } from '@angular/core';

@NgModule({

  declarations: [
    NavigationComponent,
  ],
  imports: [

  ],
  exports: [
    NavigationComponent
  ],
  providers: [
  ],
})


export class NavigationModule { }