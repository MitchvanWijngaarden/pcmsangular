import { Component } from '@angular/core';
import { User } from '@src/app/_models/User';
import { Router } from '@angular/router';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { NavbarService } from '@src/app/_services/navbar.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public nav: NavbarService)
  { }
  
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}