import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { User } from '@src/app/_models/User';
import { NavbarService } from '@src/app/_services/navbar.service';
// import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular/side-drawer-directives';
// import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {
  currentUser: User;

  // @ViewChild(RadSideDrawerComponent, { static: false });

  constructor(
    private authenticationService: AuthenticationService,
    public nav: NavbarService,
    private router: Router,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.nav.show();
  }

  ngAfterViewInit() {
    // this.drawer = this.drawerComponent.sideDrawer;
    this._changeDetectionRef.detectChanges();
  }

  
  // public openDrawer() {
  //   this.drawer.showDrawer();
  // }

  // public onCloseDrawerTap() {
  //   this.drawer.closeDrawer();
  // }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
