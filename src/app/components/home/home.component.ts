import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { User } from '@src/app/_models/User';
import { NavbarService } from '@src/app/_services/navbar.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {
  currentUser: User;

  constructor(
    private authenticationService: AuthenticationService,
    public nav: NavbarService,
    private router: Router,
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.nav.show();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
