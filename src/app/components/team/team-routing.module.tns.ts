import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { HomeComponent } from '@src/app/components/team/home/home.component';
import { MemberlistComponent } from '@src/app/components/team/memberlist/memberlist.component';
import { NewsComponent } from '@src/app/components/team/news/news.component';


const routes: Routes = [
  { path: '', redirectTo: 'team', pathMatch: 'full' },
  { path: 'team', component: HomeComponent},
  { path: 'team/members', component: MemberlistComponent},
  { path: 'news/:articleid', component: NewsComponent} 
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class TeamRoutingModule { }
