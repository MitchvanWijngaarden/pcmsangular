import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from '@src/app/components/team/team-routing.module';
import { HomeComponent } from '@src/app/components/team/home/home.component';
import { MatTableModule, MatPaginatorModule, MatTabsModule, MatIconModule, MatButtonModule, MatListModule } from '@angular/material';
import { MemberlistComponent } from '@src/app/components/team/memberlist/memberlist.component';
import { NewsComponent } from '@src/app/components/team/news/news.component';

@NgModule({
  declarations: [HomeComponent, MemberlistComponent, NewsComponent],
  imports: [
    CommonModule,
    TeamRoutingModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
  ]
})
export class TeamModule { }
