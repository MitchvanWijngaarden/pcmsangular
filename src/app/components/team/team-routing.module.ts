import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@src/app/components/team/home/home.component';
import { MemberlistComponent } from './memberlist/memberlist.component';
import { NewsComponent } from './news/news.component';


const routes: Routes = [
  { path: '', redirectTo: 'team', pathMatch: 'full' },
  { path: 'team', component: HomeComponent},
  { path: 'team/members', component: MemberlistComponent},
  { path: 'news/:articleid', component: NewsComponent} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
