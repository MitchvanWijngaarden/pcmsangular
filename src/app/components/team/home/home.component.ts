import { Component, OnInit } from '@angular/core';
import { TeamService } from '@src/app/_services/team.service';
import { Team } from '@src/app/_models/team';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { TeamUser } from '@src/app/_models/User';
import { NewsArticle } from '@src/app/_models/newsarticle';

@Component({
  selector: 'app-team-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  loadingHome = false;
  loadingMembers = false;
  loadingArticles = false;

  teamId: number;
  team: Team;
  teamMembers: TeamUser[];
  teamArticles: NewsArticle[];

  constructor(
    private teamService: TeamService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.teamId = +params.get('teamid');
    });

    this.loadingHome = true;

    this.teamService.get(this.teamId).pipe(first()).subscribe(team => {
        this.loadingHome = false;
        this.team = team;
    });

    this.loadingMembers = true;

    this.teamService.getTeamMembers(this.teamId).pipe(first()).subscribe(teamMembers => {
      this.loadingMembers = false;
      this.teamMembers = teamMembers;
    });

    this.loadingArticles = true;

    this.teamService.getTeamArticles(this.teamId).pipe(first()).subscribe(teamArticles => {
      this.loadingArticles = false;
      
      this.teamArticles = teamArticles;
    
      
      console.log(teamArticles);
    }, error => {
      console.log(error)
    });
  }


  // private getTime(date?: Date) {
  //   return date != null ? date.getTime() : 0;
  // }

  // public sortByDueDate(): void {
  //   this.teamArticles.sort((a: NewsArticle, b: NewsArticle) => {
  //       return this.getTime(a.date_published) - this.getTime(b.date_published);
  //   });
  // }
}
