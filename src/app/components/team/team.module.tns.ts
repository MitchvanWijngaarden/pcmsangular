import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { TeamRoutingModule } from '@src/app/components/team/team-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { HomeComponent } from '@src/app/components/team/home/home.component';
import { MemberlistComponent } from '@src/app/components/team/memberlist/memberlist.component';
import { NewsComponent } from '@src/app/components/team/news/news.component';


@NgModule({
  declarations: [HomeComponent, MemberlistComponent, NewsComponent],
  imports: [
    TeamRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TeamModule { }
