import { Component, OnInit } from '@angular/core';
import { NewsArticle } from '@src/app/_models/newsarticle';
import { NewsArticleService } from '@src/app/_services/news.article.service';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  loadingArticle = false;
  articleId: number;
  article: NewsArticle;
  
  constructor(
    private articleService: NewsArticleService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.articleId = +params.get('articleid');
    });
  
    this.loadingArticle = true;
    this.articleService.getArticle(this.articleId).pipe(first()).subscribe(article => {
      this.loadingArticle = false;
      this.article = article;
    });
  } 
}