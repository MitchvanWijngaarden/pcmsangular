import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { TeamUser } from '@src/app/_models/User';
import { TeamService } from '@src/app/_services/team.service';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Team } from '@src/app/_models/team';



@Component({
  selector: 'app-memberlist',
  templateUrl: './memberlist.component.html',
  styleUrls: ['./memberlist.component.css']
})

export class MemberlistComponent {

  displayedColumns: string[] = ['full_name', 'email'];
  dataSource: MatTableDataSource<TeamUser>;

  loadingHome = false;
  loadingMembers = false;

  teamId: number;
  team: Team;
  teamMembers: TeamUser[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private teamService: TeamService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngAfterViewInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      this.teamId = +params.get('teamid');
    });

    this.loadingMembers = true;

    this.teamService.getTeamMembers(this.teamId).pipe(first()).subscribe(teamMembers => {
      this.loadingMembers = false;
      this.teamMembers = teamMembers;
      this.dataSource = new MatTableDataSource<TeamUser>(this.teamMembers);
      this.dataSource.paginator = this.paginator;
  });

  }
}
