import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserFile } from '@src/app/_models/file';
import { FileService } from '@src/app/_services/file.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  loadingMyFiles: boolean;
  myFiles: UserFile[];

  constructor(
    private fileService: FileService
  ) { }

  ngOnInit() {
    this.loadingMyFiles = true;
    this.fileService.getMyFiles().pipe(first()).subscribe(files => {
      this.loadingMyFiles = false;
      this.myFiles = files;
    });;
  }
}
