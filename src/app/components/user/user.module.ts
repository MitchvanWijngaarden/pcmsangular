import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from '@src/app/components/user/user-routing.module';
import { UserComponent } from '@src/app/components/user/user.component';
import { NavigationModule } from '@src/app/components/navigation/navigation.module';
import { ProfileComponent } from '@src/app/components/user/profile/profile.component';
import { FileComponent } from '@src/app/components/user/file/file.component';
import { MatTabsModule, MatIconModule, MatButtonModule, MatListModule } from '@angular/material';

@NgModule({
  declarations: [
    UserComponent,
    ProfileComponent,
    FileComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,    
    NavigationModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule

  ]
})
export class UserModule { }
