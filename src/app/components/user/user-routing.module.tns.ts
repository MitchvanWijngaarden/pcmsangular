import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { UserComponent } from '@src/app/components/user/user.component';
import { ProfileComponent } from '@src/app/components/user/profile/profile.component';


const routes: Routes = [
  {
    path: '', component: UserComponent, children: [
      {
        path: 'dashboard', loadChildren: () => import(`@src/app/components/user/dashboard/dashboard.module`).then(m => m.DashboardModule)
      },
    ],
  }, 
  { path: 'profile/view', component: ProfileComponent},
  { path: ':user_id/profile/view', component: ProfileComponent}
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class UserRoutingModule { }
