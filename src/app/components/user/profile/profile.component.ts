import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '@src/app/_services/user.profile.service';
import { UserProfile } from '@src/app/_models/user';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Team } from '@src/app/_models/team';

@Component({
  selector: 'app-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  loadingProfile = false;
  loadingTeams = false;
  profile: UserProfile;
  teams: Team;
  userId: number;
  
  constructor(
    private userProfileService: UserProfileService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.userId = +params.get('user_id');
    });
  
    this.loadingProfile = true;
    this.userProfileService.getProfile(this.userId).pipe(first()).subscribe(profile => {
      this.loadingProfile = false;
      this.profile = profile;
    });

    this.loadingTeams = true;
    this.userProfileService.getTeams(this.userId).pipe(first()).subscribe(teams => {
      this.loadingTeams = false;
      this.teams = teams;
    });
  } 
}