import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from '@src/app/components/user/user.component';
import { ProfileComponent } from '@src/app/components/user/profile/profile.component';
import { FileComponent } from './file/file.component';


export const routes: Routes = [
  {
    path: '', component: UserComponent, children: [
      {
        path: 'dashboard', loadChildren: () => import(`@src/app/components/user/dashboard/dashboard.module`).then(m => m.DashboardModule)
      },
    ],
  }, 
  { path: 'profile/view', component: ProfileComponent},
  { path: 'files/view', component: FileComponent},
  { path: ':user_id/profile/view', component: ProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
