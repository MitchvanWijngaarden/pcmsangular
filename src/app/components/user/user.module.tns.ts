import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { UserRoutingModule } from '@src/app/components/user/user-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { UserComponent } from '@src/app/components/user/user.component';
import { ProfileComponent } from '@src/app/components/user/profile/profile.component';
import { NavigationModule } from '@src/app/components/navigation/navigation.module';
import { FileComponent } from '@src/app/components/user/file/file.component';


@NgModule({
  declarations: [
    UserComponent,
    ProfileComponent,
    FileComponent,
  ],
  imports: [
    NativeScriptCommonModule,
    UserRoutingModule,
    NavigationModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class UserModule { }
