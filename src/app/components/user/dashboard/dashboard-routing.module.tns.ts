import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { DashboardComponent } from '@src/app/components/user/dashboard/dashboard.component';
import { UserappsComponent } from '@src/app/components/user/dashboard/userapps/userapps.component';
import { TeamsComponent } from '@src/app/components/user/dashboard/teams/teams.component';
import { ManagementComponent } from '@src/app/components/user/dashboard/management/management.component';


const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      {
        path: 'apps', component: UserappsComponent
      },
      {
        path: 'teams', component: TeamsComponent
      },
      {
        path: 'manage', component: ManagementComponent
      },
    ]
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class DashboardRoutingModule { }
