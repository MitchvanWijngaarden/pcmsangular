import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from '@src/app/components/user/dashboard/dashboard-routing.module';
import { UserappsComponent } from '@src/app/components/user/dashboard/userapps/userapps.component';
import { DashboardComponent } from './dashboard.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { TeamsComponent } from './teams/teams.component';
import { ManagementComponent } from './management/management.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list'; 

@NgModule({
  declarations: [
    UserappsComponent,
    TeamsComponent,
    DashboardComponent,
    ManagementComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatToolbarModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    MatListModule
  ]
})
export class DashboardModule { }
