import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { DashboardRoutingModule } from '@src/app/components/user/dashboard/dashboard-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { UserappsComponent } from '@src/app/components/user/dashboard/userapps/userapps.component';
import { TeamsComponent } from '@src/app/components/user/dashboard/teams/teams.component';
import { ManagementComponent } from '@src/app/components/user/dashboard/management/management.component';
import { DashboardComponent } from '@src/app/components/user/dashboard/dashboard.component';

@NgModule({
  declarations: [
    UserappsComponent,
    TeamsComponent,
    DashboardComponent,
    ManagementComponent
  ],
  imports: [
    DashboardRoutingModule,
    NativeScriptCommonModule,
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule { }
