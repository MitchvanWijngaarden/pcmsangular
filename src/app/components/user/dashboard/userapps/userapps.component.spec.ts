import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserappsComponent } from '@src/app/components/user/dashboard/userapps/userapps.component';

describe('UserappsComponent', () => {
  let component: UserappsComponent;
  let fixture: ComponentFixture<UserappsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserappsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserappsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
