import { Component, OnInit } from '@angular/core';
import { TeamService } from '@src/app/_services/team.service';
import { Team } from '@src/app/_models/team';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  loadingMyTeams: boolean;
  loadingAllTeams: boolean;
  myTeams: Team[];
  allTeams: Team[];

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit() {
    this.loadingMyTeams = true;
    this.teamService.getMyTeams().pipe(first()).subscribe(teams => {
      this.loadingMyTeams = false;
      this.myTeams = teams;
    });
    this.loadingAllTeams = true;
    this.teamService.getAll().pipe(first()).subscribe(teams => {
      this.loadingAllTeams = false;
        this.allTeams = teams;
    });;
  }
}
