import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { UserappsComponent } from './userapps/userapps.component';
import { TeamsComponent } from './teams/teams.component';
import { ManagementComponent } from './management/management.component';
import { RoleGuard } from '@src/app/_helpers/role.guard';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      {
        path: 'apps', component: UserappsComponent
      },
      {
        path: 'teams', component: TeamsComponent
      },
      {
        path: 'manage',
        component: ManagementComponent,
        canActivate: [RoleGuard],
        data: {roles: [9002, ]}
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
