import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public activeTab: string;

  constructor() { }

  public tabIndexChanged(e: any) {

    switch (e.newIndex) {
        case 0:
            console.log(`Selected tab index: ${e.newIndex}`);
            break;
        case 1:
            console.log(`Selected tab index: ${e.newIndex}`);
            break;
        case 2:
              console.log(`Selected tab index: ${e.newIndex}`);
              break;
        default:
            break;
    }
}

  ngOnInit() {
  }

}
