import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@src/app/_services/authentication.service';
import { NavbarService } from '@src/app/_services/navbar.service';
import { User } from '@src/app/_models/User';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public nav: NavbarService 
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.nav.show();
  }
  
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
